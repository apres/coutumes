# Coutumes

## Animation d'une réunion
Celles-ci se feront en presentiel ou en
visioconférence sur une instance Jitsii (voir guide [ici](https://framagit.org/RomainC/jitsi))
 et on utilisera l'animation à tour de
rôle (voir détails dans le guide jitsi).
Lorsque nous auront la chance de nous rencontrez dans la vie réelle
réelle l'animation tour nera dans le sens
des aiguilles d'une montre.

## Compte-Rendu et documents écrits
On utlisera un outil collaboratif pour
rédiger un compte-rendu en [Markdown](https://framagit.org/RomainC/markdown) de la réunion. Ce moyen accessible au plus
grand nombre permettra ensuite d'enregistrer le compte-rendu.

## Utilisation de Git pour suivre les changements
On utilisera ce [Gitlab](https://framagit.org/RomainC/gitlab) pour soumettre des propositions de changement aux status ou aux coutumes grace à des *merges requests* (proposition de fusion)

## Utiliser la sortition pour désigner les membres du bureau
Une procédure est décrite [ici](https://framagit.org/RomainC/sort) pour cela. L'idée est d'utiliser la chance pour désigner les membres du bureau


